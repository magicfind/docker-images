FROM docker:latest

RUN apk add bash curl python3 openssh openssh-keygen
RUN pip3 install awscli ecs-deploy
RUN curl -sL https://sentry.io/get-cli/ | bash
